import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Linking} from 'react-native';
import FastImage from 'react-native-fast-image';

interface IUsercard {
  userImage?: string;
  userName?: string;
  userLink?: string | undefined;
}

const UserCard: React.FC<IUsercard> = ({userImage, userName, userLink}) => {
  return (
    <TouchableOpacity style={styles.defaultCard}>
      <FastImage
        style={styles.defaultImage}
        source={{uri: userImage}}
        resizeMode={FastImage.resizeMode.cover}
      />
      <View style={styles.viewUser}>
        <Text style={styles.textUser}>{userName?.toUpperCase()}</Text>
      </View>

      <TouchableOpacity
        onPress={() => Linking.openURL('https://github.com/' + userName)}>
        <Text style={styles.textUser2}>{userLink?.toUpperCase()}</Text>
      </TouchableOpacity>
    </TouchableOpacity>
  );
};

export default UserCard;

const styles = StyleSheet.create({
  defaultCard: {
    flex: 1,
    backgroundColor: 'white',
    borderRadius: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    margin: 8,
  },
  defaultImage: {
    width: '100%',
    height: 200,
    maxHeight: 200,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },
  viewUser: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 12,
  },
  textUser: {
    width: '100%',
    marginTop: 10,
    textAlign: 'center',
  },
  textUser2: {
    width: '100%',
    marginTop: 10,
    textAlign: 'center',
    color: '#FF00FF',
  },
});
