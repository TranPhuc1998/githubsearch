import React, {useEffect, useState} from 'react';
import {View, FlatList, Text} from 'react-native';
import UserCard from '../UserCard/UserCard';
// import ProductCategoryItem from '../ProductCategoryItem/ProductCategoryItem';
// import ProductItem from '../ProductItem/ProductItem';
// import {fromSearchUsersGithubApiToUI} from '../../features/SearchProducts/adapters/fromApiToUI';
// import {searchUsersGithubAsync} from '../../services/githubServices';
interface Props {
  products: Array<{
    name: string;
    avatar: string;
    link: string;
  }>;
}

const renderUsers = ({
  item,
}: {
  item: {
    name: string;
    avatar: string;
    link: string;
  };
}) => {
  return (
    <View>
      <UserCard
        userName={item.name}
        userImage={item.avatar}
        userLink={item.link}
      />
    </View>
  );
};

const ProductTable: React.FC<Props> = ({products}) => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    if (loading) {
      setTimeout(() => {
        setLoading(false);
        console.log(loading);
      }, 2000);
    }
  }, [loading]);
  return (
    <View style={{flex: 1}}>
      {/* {loading ? (
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text
            style={{
              textAlign: 'center',
            }}>
            Waitting ...
          </Text>
        </View>
      ) : (
       
      )} */}
      <FlatList
        data={products}
        renderItem={renderUsers}
        keyExtractor={(item, index) => `${item.toString()}-${index}`}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{marginTop: 18, paddingRight: 24}}
      />
    </View>
  );
};

export default ProductTable;
