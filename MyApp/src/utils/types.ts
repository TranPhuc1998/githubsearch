export interface IListFeatureItem {
  title: string;
  description: string;
}

export interface IListDish {
  id: string;
  name: string;
  items: Array<{
    id: string;
    price: number;
    image: string;
    name: string;
  }>;
}
