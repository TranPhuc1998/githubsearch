import {IListFeatureItem} from './types';
export const LIST_FEATURE: Array<IListFeatureItem> = [
  {
    title: 'GITHUB',
    description: 'Learning about Search User using API GitHub',
  },
  {
    title: 'INTERVIEW',
    description: 'Learning about Search User using API GitHub',
  },
  {
    title: 'GAME',
    description: 'Learning about animation',
  },
];
