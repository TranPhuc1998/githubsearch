import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export type cardItem = {
  id: string;
};

type items = {
  [key: string]: number;
};
const initialState = {} as items; //  {"2003": 1}

const cartSlice = createSlice({
  name: 'cart',
  initialState: initialState,
  reducers: {
    updateCard: (state, action: PayloadAction<cardItem>) => {
      const productId = action.payload;
      console.log(productId, 'hjhjhj');
      console.error(1);
      if (state[productId.id]) {
        // key and value
        state[productId.id] += 1;
      } else {
        state[productId.id] = 1;
      }
      console.log(state, 'hellovanbinh');
    },
    removeCard2: (state, action: PayloadAction<cardItem>) => {
      // đây là hàm removeCard truyền vào ID
      const productId = action.payload;
      console.log(productId, '-id');
      console.log(state[productId.id], '-12345');

      delete state[productId.id];
      return state;
    },
  },
});

export const {updateCard, removeCard2} = cartSlice.actions;
export default cartSlice.reducer;
