import {configureStore} from '@reduxjs/toolkit';
import authenticationReducer from './authenticationSlice';
import checkoutCartReducer from './checkoutCartSlice';
import infoReducer from './infoSlice';
import productReducer from './productSlice';
import cartReducer from './cardSlice';
// const store = createStore(rootReducer);

const store = configureStore({
  reducer: {
    authentication: authenticationReducer,
    checkoutCart: checkoutCartReducer,
    product: productReducer,
    info: infoReducer,
    cart: cartReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = ReturnType<typeof store.dispatch>;

export default store;
