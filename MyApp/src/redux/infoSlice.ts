import {createSlice, PayloadAction} from '@reduxjs/toolkit';

export type infoItem = {
  data: Array<{
    id: string;
    name: string;
    items: Array<{
      id: string;
      price: number;
      image: string;
      name: string;
    }>;
    quantity: number;
  }>;
  total: number;
};

const initialState = {
  data: [],
  total: 0,
} as infoItem;

const infoSlice = createSlice({
  name: 'info',
  initialState: initialState,
  reducers: {
    updateProduct: (state, action: PayloadAction<infoItem>) => {
      try {
        const infoABC = action.payload.data;
        state.data = [...infoABC];
        console.log(infoABC, '0000');
        // const total += infoABC.price*quantity;
        // state.total = total;
        return state;
      } catch (error) {
        console.log(error);
      }
    },

    addProduct: (state, action: PayloadAction<infoItem>) => {},
  },
});

export const {updateProduct} = infoSlice.actions;
export default infoSlice.reducer;
