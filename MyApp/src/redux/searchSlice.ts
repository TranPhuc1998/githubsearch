import {createSlice, PayloadAction} from '@reduxjs/toolkit';

interface SearchState {
  data: string;
}

const initialSearchState: SearchState = {
  data: '',
};

const searchSlice = createSlice({
  name: 'authentication',
  initialState: initialSearchState,
  reducers: {
    search: (state, action: PayloadAction<SearchState>) => {
      state.data = action.payload.data;
    },
  },
});

export const {search} = searchSlice.actions;
export default searchSlice.reducer;
