import {combineReducers} from 'redux';

interface AuthenticationState {
  isLogin: boolean;
  user?: {
    name: string;
    email: string;
  } | null;
  token?: string;
}

interface AuthenticationAction {
  type: 'LOGIN' | 'LOGOUT';
  payload?: AuthenticationState;
}

const initialAuthenState: AuthenticationState = {
  isLogin: false,
};

const rootReducer = combineReducers({
  authentication: (
    state: AuthenticationState = initialAuthenState,
    action: AuthenticationAction,
  ): AuthenticationState => {
    switch (action.type) {
      case 'LOGIN':
        return {
          ...state,
          isLogin: true,
          user: action.payload?.user,
          token: action.payload?.token,
        };
      case 'LOGOUT':
        return {
          ...state,
          isLogin: false,
          user: null,
          token: '',
        };
      default:
        return state;
    }
  },
});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
