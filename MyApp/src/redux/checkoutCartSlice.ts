import {createAsyncThunk, createSlice, PayloadAction} from '@reduxjs/toolkit';

// const quantityByProductIds = {
// 	'burger1': 10,
// 	'burger2': 20
// }

interface CheckoutCartState {
  cartProductIds: string[];
  quantityByProductIds: Record<string, number>;
}

const initialState: CheckoutCartState = {
  cartProductIds: [],
  quantityByProductIds: {},
};

const checkoutCartSlice = createSlice({
  name: 'checkoutCart',
  initialState: initialState,
  reducers: {
    addProductToCart: (state, action: PayloadAction<string>) => {
      const productId = action.payload;
      console.log(productId, 'productId');
      if (!state.cartProductIds.find(id => id === productId)) {
        state.cartProductIds = [...state.cartProductIds, productId];
      }
      console.log(state.cartProductIds, 'state.cartProductIds1');

      state.quantityByProductIds[productId] =
        state.quantityByProductIds?.[productId] + 1 || 1;
    },
    removeProductToCart: (state, action: PayloadAction<string>) => {
      const productDetele = action.payload;
      console.log('productDetele', productDetele);
      state.cartProductIds = [...state.cartProductIds].filter(item => {
        return item !== productDetele;
      });
      console.log(state.cartProductIds, 'state.cartProductIds2');
    },
  },
});

const checkProductIfExisted = createAsyncThunk(
  'products/checkProductIfExisted',
  async (productId: string, thunkAPI) => {
    const productExisted = await (productId => {
      return new Promise<{isExisted: boolean; id: string}>((res, rej) => {
        setTimeout(() => {
          res({
            isExisted: true,
            id: productId,
          });
        }, 1500);
      });
    })(productId);

    if (productExisted.isExisted) {
      thunkAPI.dispatch(addProductToCart(productExisted.id));
    }
  },
);

export {checkProductIfExisted};

export const {addProductToCart, removeProductToCart} =
  checkoutCartSlice.actions;
export default checkoutCartSlice.reducer;
