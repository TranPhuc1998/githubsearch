import {createSlice, PayloadAction} from '@reduxjs/toolkit';

interface AuthenticationState {
  isLogin: boolean;
  user?: {
    name: string;
    email: string;
  } | null;
  token?: string;
}

const initialAuthenState: AuthenticationState = {
  isLogin: false,
};

const authenticationSlice = createSlice({
  name: 'authentication',
  initialState: initialAuthenState,
  reducers: {
    login: (state, action: PayloadAction<AuthenticationState>) => {
      state.isLogin = true;
      state.user = action.payload.user;
      state.token = action.payload.token;
    },
    logout: state => {
      state.isLogin = false;
      state.user = null;
      state.token = '';
    },
  },
});

export const {login, logout} = authenticationSlice.actions;
export default authenticationSlice.reducer;
