import {SearchUsersGithubApi} from '../../../services/githubTypes';

export function fromSearchUsersGithubApiToUI(
  data: SearchUsersGithubApi.SearchUsersGithubResponse,
) {
  return data.items.map(item => {
    return {
      name: item.login,
      avatar: item.avatar_url,
      link: item.url,
    };
  });
}
