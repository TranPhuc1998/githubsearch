import React, {useEffect, useState} from 'react';
import {SafeAreaView, Text, View, ActivityIndicator} from 'react-native';

import ProductTable from '../../../components/ProductTable/ProductTable';

import SearchBar from '../../../components/SearchBar/SearchBar';

import debounce from 'lodash/debounce';
import useSearchUsersAPI from '../hooks/useSearchUsersAPI';

const DELAY_DEBOUNCE_IN_MS = 1000;

const handler = (value: string, callback: (newValue: string) => void) => {
  callback(value);
};

const debounceSearchUser = debounce(handler, DELAY_DEBOUNCE_IN_MS);

const FilterableProductTable = () => {
  const {setSearch, error, users, loading} = useSearchUsersAPI();

  const handleOnChangeText = (value: string) => {
    debounceSearchUser(value, (newValue: string) => {
      setSearch(newValue);
    });
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{flex: 1}}>
        {loading && (
          <ActivityIndicator
            color={'#EE82EE'}
            size="large"
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row',
            }}
          />
        )}

        <SearchBar
          // onChangeText chỉ nhận 1 cái text
          onChangeText={handleOnChangeText}
          placeholder="Search..."
        />

        <ProductTable products={users} />
      </View>
    </SafeAreaView>
  );
};

export default FilterableProductTable;
