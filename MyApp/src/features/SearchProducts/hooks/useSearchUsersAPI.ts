import {useEffect, useState} from 'react';
import {searchUsersGithubAsync} from '../../../services/githubServices';
import {fromSearchUsersGithubApiToUI} from '../adapters/fromApiToUI';

function useSearchUsersAPI() {
  const [users, setUsers] = useState<
    Array<{
      name: string;
      avatar: string;
      link: string;
    }>
  >([]);

  const [search, setSearch] = useState<string>('');
  const [error, setError] = useState('');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (search) {
      const fetchAPI = async (data: string) => {
        try {
          setLoading(true);
          const respUsers = await searchUsersGithubAsync({
            q: data,
            page: 1,
            per_page: 10,
            sort: 'followers',
            order: 'asc',
          });
          const mappedData = fromSearchUsersGithubApiToUI(respUsers);
          setUsers(mappedData);
        } catch (err) {
          setError((err as Error)?.message || 'Something went wrong');
        } finally {
          // code finally
          setLoading(false);
        }
      };

      fetchAPI(search);
      setLoading(true);
    }
  }, [search]);

  return {
    setSearch,
    error,
    users,
    loading,
    setLoading,
  };
}

export default useSearchUsersAPI;
