import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {updateProduct} from '../../../redux/infoSlice';
import {updateProductByIds} from '../../../redux/productSlice';
import {RootState} from '../../../redux/store';
import {productAsync} from '../../../services/productServices';
import {productApi} from '../../../services/productTypes';

const productListAPI = () => {
  const [error, setError] = useState('');

  const [dataProduct, setDataProduct] = useState<
    productApi.ProductResponse[] | []
  >([]);

  const [dataProductItems, setDataProductItems] = useState<
    productApi.ProductResponse['items']
  >([]);

  const testCase = useSelector((state: RootState) => state.info);
  useEffect(() => {
    console.log(testCase, 'hello123444');
  }, [testCase]);
  const [totalVal, setTotalValue] = useState(0);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchAPI = async () => {
      try {
        const resProps = await productAsync();
        console.log(resProps, 'PhucMy2');

        let productItems: any = [];
        for (const item of resProps) {
          productItems = productItems.concat(item.items);
        }

        dispatch(updateProductByIds(productItems));

        // const action = updateProduct({data: productItems});
        // dispatch(action);

        setDataProduct(resProps);
        setDataProductItems(resProps[0]?.items);
      } catch (error) {
        setError((error as Error)?.message || 'Something went wrong');
      }
    };
    fetchAPI();
    // resProps.reduce((acc: any, cur, idx) => {
    //   acc = acc.concat(acc, cur.items);
    //   return acc;
    // }, []),
    console.log(fetchAPI, 'PhucMy');
  }, []);
  return {
    error,
    dataProduct,
    setDataProduct,
    totalVal,
    setTotalValue,
    setDataProductItems,
    dataProductItems,
  };
};

export default productListAPI;
