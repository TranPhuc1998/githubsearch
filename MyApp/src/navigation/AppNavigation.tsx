import React, {useEffect} from 'react';
import {Button, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {AppearanceProvider} from 'react-native-appearance';
import HomeStackNavigator from './HomeStack/HomeStackNavigator';
import {useDispatch, useSelector} from 'react-redux';
import {ApplicationProvider} from '@ui-kitten/components';
import {login} from '../redux/authenticationSlice';
import {RootState} from '../redux/store';
import * as eva from '@eva-design/eva';

export type RootStackParams = {
  HomeStackNavigator: undefined;
};

const Stack = createStackNavigator<RootStackParams>();

const AppNavigation = () => {
  // const handleButton = () => {
  //   dispatch(
  //     login({
  //       isLogin: true,
  //       user: {
  //         name: 'Trần Hồng Phúc',
  //         email: 'PhucHong@gmail.com',
  //       },
  //       token: 'ADDADA',
  //     }),
  //   );
  // };

  // if (!authentication.isLogin) {
  //   return (
  //     <View
  //       style={{
  //         flex: 1,
  //         justifyContent: 'center',
  //       }}>
  //       <Text>{'Please login'}</Text>

  //       <Button
  //         onPress={() => {
  //           handleButton();
  //         }}
  //         title="Login"
  //       />
  //     </View>
  //   );
  // }

  return (
    <SafeAreaProvider>
      <AppearanceProvider>
        <ApplicationProvider {...eva} theme={eva.light}>
          <NavigationContainer>
            <Stack.Navigator
              screenOptions={{
                headerShown: false,
              }}>
              <Stack.Screen
                name={'HomeStackNavigator'}
                component={HomeStackNavigator}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </ApplicationProvider>
      </AppearanceProvider>
    </SafeAreaProvider>
  );
};

export default AppNavigation;
