import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
//import Screen
import HomeScreen from '../../screens/HomeScreen';
import SearchProductsScreen from '../../screens/SearchProductsScreen';
import ProductScreen from '../../screens/ProductScreen';
import DetailScreen from '../../screens/DetailScreen';
import GameScreen from '../../screens/GameScreen';

export type HomeStackParams = {
  HomeScreen: undefined;
  SearchProductsScreen: undefined;
  ProductScreen: undefined;
  DetailScreen: undefined;
  GameScreen: undefined;
};

const HomeStack = createStackNavigator<HomeStackParams>();

const HomeStackNavigator = () => (
  <HomeStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <HomeStack.Screen name="HomeScreen" component={HomeScreen} />
    <HomeStack.Screen
      name="SearchProductsScreen"
      component={SearchProductsScreen}
    />
    <HomeStack.Screen name="GameScreen" component={GameScreen} />
    <HomeStack.Screen name="ProductScreen" component={ProductScreen} />
    <HomeStack.Screen name="DetailScreen" component={DetailScreen} />
  </HomeStack.Navigator>
);

export default HomeStackNavigator;
