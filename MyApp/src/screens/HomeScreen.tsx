import {Layout, Text} from '@ui-kitten/components';
import React, {useState} from 'react';
import {SafeAreaView, StyleSheet, View, Button, FlatList} from 'react-native';
import MyCard from '../components/Card/MyCard';
// import Components
import SearchBar from '../components/SearchBar/SearchBar';

// import themes
import {hScale, wScale} from '../themes/Responsive';
import {LIST_FEATURE} from '../utils/mockupData';
import {IListFeatureItem} from '../utils/types';

//import component

const HomeScreen: React.FC<any> = ({navigation}) => {
  const renderItem = ({
    item,
    index,
  }: {
    item: IListFeatureItem;
    index: number;
  }) => {
    const handleOnPress = () => {
      switch (index) {
        case 0:
          navigation.navigate('SearchProductsScreen');
          break;
        case 1:
          navigation.navigate('ProductScreen');
          break;
        case 2:
          navigation.navigate('GameScreen');
          break;
      }
    };

    return (
      <MyCard
        key={`${item.title}${index}`}
        header={props => (
          <View>
            <Text {...props} category="h5">
              {`${index + 1}. ${item.title}`}
            </Text>
          </View>
        )}
        onPress={handleOnPress}>
        <Text>{item.description}</Text>
      </MyCard>
    );
  };

  return (
    <Layout>
      <View style={{paddingTop: hScale(50)}}>
        <FlatList
          data={LIST_FEATURE}
          renderItem={renderItem}
          keyExtractor={(item, index) => `${item.toString()}-${index}`}
        />
      </View>
    </Layout>
  );
};

export default HomeScreen;
