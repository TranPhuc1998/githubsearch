import React, {useEffect, useState} from 'react';
import {Button, FlatList, StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import products from '../features/SearchProducts/mocks/products';
import {removeCard2} from '../redux/cardSlice';
import {
  addProductToCart,
  removeProductToCart,
} from '../redux/checkoutCartSlice';
import {RootState} from '../redux/store';
import {productApi} from '../services/productTypes';
import {hScale, wScale} from '../themes/Responsive';
const DetailScreen = () => {
  const checkoutCart = useSelector((state: RootState) => state.checkoutCart);

  const product = useSelector((state: RootState) => state.product);

  const cart = useSelector((state: RootState) => state.cart);
  const dispatch = useDispatch();

  const [test, setTest] = useState<any>([]);

  const quantity = checkoutCart.quantityByProductIds;
  useEffect(() => {
    // console.log(product, 'conmeno');
    const data = product.productByIds;
    console.log('data', data);
    const result = checkoutCart?.cartProductIds.map((item, index) => {
      if (data[item]) {
        return data[item];
      }
    });

    setTest(result);

    console.log(result, 'conmeno');
  }, [checkoutCart]);
  // useEffect(() => {
  //   // console.log(product, 'conmeno');
  //   const data = product.productByIds;
  //   console.log('data', data);
  //   const listKey = Object.keys(cart);
  //   const result = listKey.map((item, index) => {
  //     if (data[item]) {
  //       return data[item];
  //     }
  //   });

  //   setTest(result);

  //   console.log(result, 'conmeno');
  // }, [cart]);
  const renderDetailItems = ({
    item,
    index,
  }: {
    item: productApi.ProductItem;
    index: number;
  }) => {
    return (
      <View style={{marginTop: hScale(20)}}>
        <View
          style={{
            flexDirection: 'row',
            width: wScale(400),
            justifyContent: 'space-between',
          }}>
          <Text style={{width: '33%', marginLeft: hScale(20)}}>
            {item.name}
          </Text>
          <Text style={{width: '33%'}}>{item.price * quantity[item.id]}</Text>
          <Text style={{width: '33%'}}>{quantity[item.id]}</Text>
          {/* <Text style={{width: '33%'}}>{item.price * cart[item.id]}</Text>
          <Text style={{width: '33%'}}>{cart[item.id]}</Text> */}
        </View>
        <Button
          title="Delete"
          onPress={() => {
            console.log(item.id);
            // đây là lúc gọi
            dispatch(removeProductToCart(item.id));
          }}
        />
      </View>
    );
  };

  return (
    <View style={[styles.container]}>
      <View
        style={{
          flex: 1,
          paddingTop: hScale(80),
        }}>
        <View
          style={{
            flexDirection: 'row',
            width: wScale(400),
            justifyContent: 'space-between',
          }}>
          <Text
            style={{
              width: '33%',
              marginLeft: hScale(20),
              alignItems: 'center',
            }}>
            Name
          </Text>
          <Text style={{width: '33%'}}>Price</Text>
          <Text style={{width: '33%'}}>Quantity</Text>
        </View>
        <FlatList
          data={test}
          keyExtractor={items => items.id.toString()}
          renderItem={renderDetailItems}
        />
      </View>
    </View>
    // <View
    //   style={{
    //     flex: 1,
    //     backgroundColor: 'purple',
    //   }}>
    //
    //   <View style={{justifyContent: 'center', alignItems: 'center'}}>
    //     <Text>Hello</Text>
    //   </View>

    // </View>
  );
};

export default DetailScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
