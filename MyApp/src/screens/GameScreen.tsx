import {
  Alert,
  Button,
  Modal,
  Pressable,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import React, {useState} from 'react';
import SearchBar from '../components/SearchBar/SearchBar';
import KeyboardAvoidWrapper from '../components/KeyboardAvoidWrapper/KeyboardAvoidWrapper';
import {hScale} from '../themes/Responsive';
import {useEffect} from 'react';
import {studentAsync} from '../services/studentServices';

const GameScreen: React.FC<any> = ({navigation}) => {
  const [name, setName] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [error, setError] = useState('');
  // function
  const onSubmit = () => {};

  const handleName = (value: string) => {
    setName(value);
  };

  useEffect(() => {
    console.log('hELLO');
    const fetchAPI = async () => {
      try {
        console.log('HAHH');
        const resProps = await studentAsync();
        console.log(resProps.data.getStudents);
      } catch (error) {
        setError((error as Error)?.message || 'Something went wrong');
      }
    };
    fetchAPI();
  }, []);

  return (
    <View style={styles.gameScreen}>
      <KeyboardAvoidWrapper>
        <Text style={{color: 'red', paddingTop: 30}}>GameScreen</Text>
        <TextInput
          placeholder="Nhap Ten"
          value={name}
          onChangeText={handleName}
        />
        <View style={styles.buttonSubmit}>
          <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
              Alert.alert('Modal has been closed.');
              setModalVisible(!modalVisible);
            }}>
            <View style={{paddingTop: hScale(105)}}>
              <View style={styles.modalView}>
                <Text style={styles.modalText}>
                  Hello Worldsdadasdasdadads! {name}
                </Text>
                <TouchableOpacity
                  onPress={() => navigation.navigate('DetailGameScreen')}
                  onPressOut={() => setModalVisible(false)}>
                  <Text>Move on</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <TouchableOpacity onPress={() => setModalVisible(true)}>
            <Text style={{}}>Submit</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidWrapper>
    </View>
  );
};

export default GameScreen;

const styles = StyleSheet.create({
  gameScreen: {
    paddingTop: hScale(10),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 20,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
});
