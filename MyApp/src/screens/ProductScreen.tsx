import React, {useEffect, useState} from 'react';
import {
  Button,
  Dimensions,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  FlatList,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

// import API
import productListAPI from '../features/SearchProducts/hooks/productListAPI';
import {
  addProductToCart,
  checkProductIfExisted,
} from '../redux/checkoutCartSlice';
import {productApi} from '../services/productTypes';
import {RootState} from '../redux/store';

// import utils
import {fontScale, hScale, wScale} from '../themes/Responsive';
const width = Dimensions.get('window').width;
// import Libs
import {updateCard} from '../redux/cardSlice';

const ProductScreen: React.FC<any> = ({navigation}) => {
  const {
    error,
    dataProduct,
    totalVal,
    setDataProduct,
    setTotalValue,
    dataProductItems,
    setDataProductItems,
  } = productListAPI();
  console.log(dataProduct, 'djhfakjfkaj');

  const [pick, setPick] = useState(0);
  const [typeIndex, setTypeIndex] = useState(0); // index từng loại bánh

  const dispatch = useDispatch();

  const checkoutCart = useSelector((state: RootState) => state.checkoutCart);
  const product = useSelector((state: RootState) => state.product);
  console.log('TCL: checkoutCart', checkoutCart.cartProductIds);
  console.log(checkoutCart.quantityByProductIds);

  useEffect(() => {
    totalCart();
  }, [dataProduct[typeIndex]]);

  const renderItem = ({item}: {item: productApi.ProductItem}) => {
    return (
      <TouchableOpacity
        style={styles.renderItem}
        onPress={() => {
          dispatch(addProductToCart(item.id));
          console.log(
            'TCL: renderItem -> addProductToCart(item.id)',
            addProductToCart(item.id),
          );

          // dispatch(checkProductIfExisted(item.id));

          // dispatch(
          //   updateCard({
          //     id: item.id,
          //   }),
          // );
        }}>
        <View style={{margin: 10, flexDirection: 'row'}}>
          <Image
            source={{uri: item.image ? item.image : null}}
            resizeMode="contain"
            style={styles.image}
          />
          <View style={{justifyContent: 'center'}}>
            <Text style={[{fontSize: 20, fontWeight: 'bold', color: 'black'}]}>
              {item.name}
            </Text>
            <Text
              style={{
                color: 'black',
                textAlign: 'center',
                fontWeight: 'bold',
              }}>
              ${item.price}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const renderFlatList = () => {
    return (
      <FlatList
        data={dataProductItems}
        keyExtractor={items => items.id.toString()}
        renderItem={renderItem}
      />
    );
  };

  //
  const filterType = (id: string) => {
    const newTypeIndex = dataProduct.findIndex(d => d.id === id);

    const filteredDataProduct = dataProduct.filter(
      dataItem => dataItem.id === id,
    );

    const filteredItems = filteredDataProduct[0].items;

    setDataProductItems(filteredItems);

    setTypeIndex(newTypeIndex);
    console.log(newTypeIndex, 'hello');
  };

  const totalCart = () => {
    let total = 0;
    dataProduct[typeIndex]?.items.map(item => (total += item.price));
    setTotalValue(total);
  };
  return (
    <View style={styles.productScreen}>
      <View style={{flex: 5}}>
        <Button
          title="Product"
          onPress={() => {
            navigation.goBack();
          }}
        />
        <View style={styles.productBody}>
          {dataProduct?.map(
            (item: productApi.ProductResponse, index: number) => {
              return (
                <View style={styles.productBodyText}>
                  <TouchableOpacity
                    onPress={() => {
                      setPick(index);
                      filterType(item.id);
                    }}
                    key={item.id}>
                    {index === pick ? (
                      <View style={styles.productBodyText}>
                        <Text key={index} style={styles.text}>
                          {item.name}({item.items.length})
                        </Text>
                        <View style={styles.productDot} />
                      </View>
                    ) : (
                      <Text key={index} style={styles.text}>
                        {item.name}({item.items.length})
                      </Text>
                    )}
                  </TouchableOpacity>
                </View>
              );
            },
          )}
        </View>
        <View style={styles.divider} />
        {dataProduct[typeIndex]?.items.length > 0 && (
          <View
            style={{
              flex: 1,
              paddingTop: hScale(20),
            }}>
            {renderFlatList()}
          </View>
        )}
      </View>

      <TouchableOpacity
        style={styles.toTalTouch}
        onPress={() => {
          Alert.alert('Hello');
          navigation.navigate('DetailScreen');
        }}>
        <View style={styles.toTal}>
          <Text>
            Tổng Tiền:{' '}
            {Object.keys(checkoutCart.quantityByProductIds).reduce(
              (total, key) => {
                return checkoutCart.quantityByProductIds[key] + total;
              },
              0,
            )}{' '}
            {Object.keys(checkoutCart.quantityByProductIds).reduce(
              (total, key) => {
                return (
                  total +
                  product.productByIds[key].price *
                    checkoutCart.quantityByProductIds[key]
                );
              },
              0,
            )}
            $
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default ProductScreen;

const styles = StyleSheet.create({
  productScreen: {
    flex: 5,
    backgroundColor: '#fff',
    paddingLeft: wScale(30),
    paddingTop: wScale(35),
    paddingRight: wScale(30),
  },
  productBodyText: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  productBody: {
    flexDirection: 'row',
  },
  text: {
    paddingRight: hScale(20),
    fontWeight: 'bold',
  },
  divider: {
    borderBottomColor: 'black',
    borderBottomWidth: 0.5,
    paddingTop: hScale(10),
    alignSelf: 'stretch',
  },
  productDot: {
    width: wScale(8),
    height: hScale(8),
    backgroundColor: '#FB6D3B',
    borderRadius: 4,
    marginTop: 5,
  },
  renderItem: {
    paddingTop: hScale(30),
    paddingRight: hScale(250),
    borderRadius: 10,
    borderWidth: 0.85,
    borderColor: '#000',
  },
  image: {
    width: wScale(200),
    height: hScale(100),
  },
  toTalTouch: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  toTal: {
    width: wScale(300),
    height: hScale(50),
    borderRadius: 10,
    borderWidth: 0.85,
    borderColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
