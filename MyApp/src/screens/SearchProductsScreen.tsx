import React from 'react';

import FilterableProductTable from '../features/SearchProducts/components/FilterableProductTable';

const SearchProductsScreen = () => {
  return <FilterableProductTable />;
};

export default SearchProductsScreen;
