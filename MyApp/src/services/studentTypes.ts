export declare module studentApi {
  interface studentItem {
    data: {
      getStudents: {
        email: string;
        fullname: string;
        id: number;
      }[];
    };
  }
}
