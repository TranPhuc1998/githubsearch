import {ImageProps} from 'react-native';

export declare module productApi {
  interface ProductItem {
    id: string;
    name: string;
    price: number;
    image: ImageProps | Readonly<ImageProps>;
  }
  interface ProductResponse {
    id: string;
    name: string;
    items: ProductItem[];
  }
}
