import axios from 'axios';

// Create an Axios Instance
// baseURL - full URL of the server
const studentAxiosInstance = axios.create({
  baseURL: 'http://localhost:3000/api/v1/graphQl',
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json;charset=UTF-8',
    // Authorization: 'Bearer '
  },
});

export default studentAxiosInstance;
