import productAxiosInstance from '../services/productHttpClient';

import {productApi} from '../services/productTypes';

export async function productAsync() {
  const response = await productAxiosInstance.get<productApi.ProductResponse[]>(
    '',
    {},
  );
  // console.log(response, 'hello');
  return response.data;
}
