import axios from 'axios';

// Create an Axios Instance
// baseURL - full URL of the server
const productAxiosInstance = axios.create({
  baseURL:
    'https://gist.githubusercontent.com/trandongtam/21b7633d121e6e72d1afcc603f484fe5/raw/f9e8558f62d854715fc63fc9eafaafb78d68e7c8/data.json',
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json;charset=UTF-8',
    // Authorization: 'Bearer '
  },
});

export default productAxiosInstance;
