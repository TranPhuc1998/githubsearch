import studentAxiosInstance from './studentHttpClient';
import {studentApi} from './studentTypes';
import gql from 'graphql-tag';

const get_Student = `
  query getStudents {
    getStudents {
      fullname
      email
      id
    }
  }
`;

export async function studentAsync() {
  const response = await studentAxiosInstance.post<studentApi.studentItem>('', {
    query: get_Student,
  });
  // console.log(response, 'hello');
  return response.data;
}
