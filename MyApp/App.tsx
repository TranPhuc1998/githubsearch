/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import 'react-native-gesture-handler';
import AppNavigation from './src/navigation/AppNavigation';
import {Provider} from 'react-redux';

import store from './src/redux/store';

//import Redux

const App = (): JSX.Element => {
  return (
    <Provider store={store}>
      <AppNavigation />
    </Provider>
  );
};

export default App;
